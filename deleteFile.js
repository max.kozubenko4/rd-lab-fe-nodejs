const express = require('express');
const router = express.Router();
const path = require('path');
const fs = require('fs');

router.get(/(\/api\/files\/delete\/)\w+\.(log|txt|json|yaml|xml|js)+$/, function (req, res) {
    const filePath = req.url.slice(req.url.lastIndexOf('/') + 1);
    const mainDir = './files/';

    if (fs.existsSync(mainDir + filePath)) {
        fs.unlink(mainDir + filePath, function (err) {
            if(err) {
                file.message = `Server error`;
                res.status(500).send(`Server error`);
            };

            res.status(200).send(`File ${filePath} has deleted`);
        });
    } else {
        res.status(400).send(`No file with ${filePath} filename found`);
    }
});

module.exports = { 
    deleteFile: router, 
}
