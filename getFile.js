const express = require('express');
const router = express.Router();
const path = require('path');
const fs = require('fs');

router.get(/(\/api\/files\/)\w+\.(log|txt|json|yaml|xml|js)+$/, function (req, res) {
    const filePath = req.url.slice(req.url.lastIndexOf('/') + 1);
    const mainDir = './files/';

    const file = {
        message: "Success",
        filename: filePath,
        content: "",
        extension: path.extname(filePath),
        uploadedDate: ""
    };

    if (fs.existsSync(mainDir + filePath)) {
        const { birthtime } = fs.statSync(mainDir + filePath);
        file.uploadedDate = birthtime;
        fs.readFile(mainDir + filePath, 'utf8', (error,data) => {
            if(error) {
                file.message = `Server error`;
                res.status(500).send(`Server error`);
            };
            file.content = data;
            res.status(200).send(`${file.message}  ${file.filename}  ${file.content}  ${file.extension}  ${file.uploadedDate}`);
        });
    } else {
        file.message = `No file with ${filePath} filename found`;
        res.status(400).send(file.message);
    }
});

module.exports = { 
    getFile: router, 
}
