const express = require('express');
const router = express.Router();
const fs = require('fs');

router.post('/api/files', function (req, res) {
    let { filename, content } = req.body;

    if(!content) {
        res.status(400).send(`Please specify 'content' parameter`);
    }

    if(/\w+\.(log|txt|json|yaml|xml|js)+$/i.test(filename)) {
        fs.writeFile('./files/' + filename, content, (err) => {
            if(err) {
               throw res.status(500).send(`Server error`);
            }
        });
    }
    
    res.status(200).send(`File created successfully`);
});

module.exports = { 
    createFile: router, 
}
