const express = require('express');
const router = express.Router();

const testFolder = './files/';
const fs = require('fs');

router.get('/api/files', function (req, res) {
    const fileArr = {
        message: "Success",
        files: []
    };
    const mainDir = './files/';

    if (fs.existsSync(mainDir)) {
        fs.readdir(testFolder, (err, files) => {
            if(err) {
                throw res.status(500).send(`Server error`);
            }
            files.forEach(file => {
                fileArr.files.push(file);
            });
    
            res.status(200).send(`${fileArr.message} ${fileArr.files}`);
        });
    } else {
        res.status(400).send('Client error');
    }    
});

module.exports = { 
    getFiles: router, 
}
