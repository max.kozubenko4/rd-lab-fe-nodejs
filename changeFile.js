const express = require('express');
const router = express.Router();
const path = require('path');
const fs = require('fs');

router.post(/(\/api\/files\/change)/, function (req, res) {
    const mainDir = './files/';

    let { filename, content } = req.body;

    if(!content) {
        res.status(400).send(`Please specify 'content' parameter`);
    }

    if (fs.existsSync(mainDir + filename)) {
        fs.writeFile(mainDir + filename, content, (err) => {
            if(err) {
                throw res.status(500).send(`Server error`);
            }
            res.status(200).send(`File changed successfully`);
        });
    } else {
        res.status(400).send(`No file with ${filename} filename found`);
    }
});

module.exports = { 
    changeFile: router, 
}
